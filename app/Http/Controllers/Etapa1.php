<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\Response;
use Illuminate\Contracts\Session\Session;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class Etapa1 extends Controller
{

       /**
     * Display a listing of the resource.
     *
     * @return \I
     * lluminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['title']='primeros pasos';
        $data['filtros']=$filtros=[1,2,3,4];
        $random=array();
        foreach (range(0, 6) as $i) {
            $random[$i] = array();
            foreach (range(0, 6) as $number) {
                while (($l=rand(0,1) / getrandmax() *3.0* pow(10,9))==0);
                $random[$i][$number] = $l;
            }
        }
        $request->session()->put('random', $random);

        $data['random']=$random;
       // dd($random);

        return view('inicial',$data)->nest('headnav','headnav',$data)->nest('sidebar','sidebar',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function calculo(Request $request)
    {
        $data['title']='primeros pasos';
        $randoms=$request->session()->get('random');
        if(!isset($randoms))
            return redirect('/');
        $request=$request->all();
        $i=0;
        $values=array();
        foreach ($randoms as $item) {
            $values[$i]=(int)$item[0]*$request['select1'];
            $i++;
        }
    $data['values']=$values;
        $max = -100;
        $prom=0;
        foreach ($randoms as $random) {
            $count=count($random);
            foreach ($random as $item) {
                $prom+= $item/($count);
                if ($item > $max) {
                    $max = $item;
                }
            }
        }
        $suma=0;
        $mult=1;
        foreach ($values as $value) {
         $suma+=$value;
            $mult*=$value;
        }
        $data['suma']=$suma;
        $data['mult']=$mult;
        $data['max']=$max;
        $data['prom']=$prom;
        $data['potencia']= $request['select1']*$request['select2'];
        //dd($data);
        return view('calculo',$data)->nest('headnav','headnav',$data)->nest('sidebar','sidebar',$data);

    }
}
