@extends('base')

@section('tittle')
    {{$title}}
@endsection

@section('end-head')

@endsection

@section('headnav')
    {!! $headnav !!}
@endsection


@section('sidebar')
    {!! $sidebar !!}
@endsection

@section('content')
    <div id="content" class="span10">


        <ul class="breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="index.html">Home</a>
                <i class="icon-angle-right"></i>
            </li>
            <li><a href="#">Charts</a></li>
        </ul>

        <div class="row-fluid sortable">
            <div class="box">
                <div class="box-header">
                    <h2><i class="halflings-icon white list-alt"></i><span class="break"></span>Chart with points</h2>
                    <div class="box-icon">
                        <a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
                        <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                        <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
                    </div>
                </div>
                <div  class="box-content">
                    <div class="form-group">
                    <form id="form" action="{{url('/calcular')}}" method="get">

                        <div class="control-group">
                        <label class="control-label" for="selectError1">Dato 1</label>
                        <div class="controls">
                            <select id="selectError1" name="select1" data-rel="chosen">
                                @foreach($filtros as $filtro)<option value="{{$filtro}}">{{$filtro}}</option>@endforeach
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="selectError2">Dato2</label>
                        <div class="controls">
                            <select id="selectError2"  name="select2" data-rel="chosen">
                                @foreach($filtros as $filtro)<option value="{{$filtro}}">{{$filtro}}</option>@endforeach
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="selectError3">Dato 3</label>
                        <div class="controls">
                            <select id="selectError3" name="select3" data-rel="chosen">
                                @foreach($filtros as $filtro)<option value="{{$filtro}}">{{$filtro}}</option>@endforeach
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"  for="selectError4">Dato 4</label>
                        <div class="controls">
                            <select id="selectError4" name="select4" data-rel="chosen">
                                @foreach($filtros as $filtro)<option value="{{$filtro}}">{{$filtro}}</option>@endforeach
                            </select>
                        </div>
                    </div>
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </form>
                </div><!-- form-group-->
            </div><!--box content-->
            </div><!--box-->


        </div><!--/row-->



    </div>
    <!-- end content.span10-->




<div class="clearfix"></div>
@endsection

@section('end-script')

@endsection