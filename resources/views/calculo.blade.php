@extends('base')

@section('tittle')
    {{$title}}
@endsection

@section('end-head')

@endsection

@section('headnav')
    {!! $headnav !!}
@endsection


@section('sidebar')
    {!! $sidebar !!}
@endsection

@section('content')
    <div id="content" class="span10">


        <ul class="breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="index.html">Home</a>
                <i class="icon-angle-right"></i>
            </li>
            <li><a href="#">Charts</a></li>
        </ul>

        <div class="row-fluid sortable">
            <div class="box">
                <div class="box-header">
                    <h2><i class="halflings-icon white list-alt"></i><span class="break"></span>Chart with points</h2>
                    <div class="box-icon">
                        <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                        <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
                    </div>
                </div>
                <div  class="box-content">
                    <div id="my_chart"></div>
                </div><!--box content-->

            </div><!--box-->

        </div><!--/row-->
        <div class="row-fluid sortable">
            <div class="box span12">
                <div class="box-header" >
                    <h2><i class="halflings-icon white user"></i><span class="break"></span>Resultados</h2>
                    <div class="box-icon">
                        <a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
                        <a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
                    </div>
                </div>
                <div class="box-content">
                    <table class="table table-striped table-bordered bootstrap-datatable datatable">
                        <thead>
                        <tr>
                            <th>Dato</th>
                            <th>Valor</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Suma</td>
                            <td class="center">{{$suma}}</td>
                        </tr>
                        <tr>
                            <td>Potencia</td>
                            <td class="center">{{$potencia}}</td>
                        </tr>
                        <tr>
                            <td>Promedio</td>
                            <td class="center">{{$prom}}</td>
                        </tr>
                        <tr>
                            <td>Max.</td>
                            <td class="center">{{$max}}</td>
                        </tr>



                        </tbody>
                    </table>
                </div>
            </div><!--/span-->

        </div><!--/row-->



    </div>
    <!-- end content.span10-->




    <div class="clearfix"></div>
@endsection

@section('end-script')

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script>


        google.charts.load('current', {'packages':['corechart']});

        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Filtro');
            data.addColumn('number', 'valor');
            data.addRows([
                    @foreach($values as $key=>$value)
                ['{{$key}}',{{$value}}],
                @endforeach
            ]);

            var options = {
                'title':'Grafico 1',
                'width':400,
                'height':300,
                hAxis:{title:'valores'},
                vAxis:{title: 'filtros'},
            };

            var chart = new google.visualization.LineChart(document.getElementById('my_chart'));

            chart.draw(data,options);
        };


    </script>
@endsection